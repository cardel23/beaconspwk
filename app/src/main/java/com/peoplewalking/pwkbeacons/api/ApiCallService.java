package com.peoplewalking.pwkbeacons.api;

import com.peoplewalking.pwkbeacons.model.AppUser;
import com.peoplewalking.pwkbeacons.model.Beacon;
import com.peoplewalking.pwkbeacons.model.Record;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;


/**
 * Created by pwk04 on 05-17-17.
 */

public interface ApiCallService {
    String REST_BASE_URL = "beacons/api/rest";

    @GET(REST_BASE_URL+"/record/{id}")
    Call<Record> getRecord(@Path("id") String recordId);

    @POST(REST_BASE_URL+"/record")
    Call<Record> sendRecord(@Body Record record);

    @GET(REST_BASE_URL+"/beacon/{id}")
    Call<Beacon> getBeacon(@Path("id") String beaconId);

    @POST(REST_BASE_URL+"/beacon/create")
    Call<Beacon> sendBeacon(@Body Beacon beacon);

    @GET(REST_BASE_URL+"/beacon/byMac/{id}")
    Call<Beacon> getBeaconByMac(@Path("id") String mac);

    @GET(REST_BASE_URL+"/beacon/solo/")
    Call<Beacon> getBeacon();

    @GET(REST_BASE_URL+"/beacon/")
    Call<List<Beacon>> getBeacons();

    @GET(REST_BASE_URL+"/user/byName/{name}")
    Call<AppUser> getUser(@Path("name") String name);

    @POST(REST_BASE_URL+"/user/create")
    Call<AppUser> sendUser(@Body AppUser appUser);
}
