package com.peoplewalking.pwkbeacons.api;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peoplewalking.pwkbeacons.config.AppContext;
import com.peoplewalking.pwkbeacons.model.Record;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by pwk04 on 05-17-17.
 */

public class ApiCallServiceClient {

    private static AppContext app;
    private static ApiCallService apiCallService;
    private static String serviceURL;

    public static String OB_SERVER;
    private static String OB_PORT;
    public static String OB_URL = "http://"+OB_SERVER+"/openbravo/";
    //    	public static final String OB_SERVER = "http://52.16.177.26/openbravo";
//    	public static final String OB_SERVER = "http://192.168.10.104:8080/openbravo";
    public static final String REST_BASE_URL = OB_SERVER + "org.openbravo.service.json.jsonrest/";
    public static final String SHOWIMAGE_URL = OB_SERVER + "utility/ShowImage";

    public ApiCallServiceClient(Context context){
        app = (AppContext) context;
        StringBuilder serviceUrlBuilder = new StringBuilder();
        serviceUrlBuilder.append("http://")
        .append(app.getServerFromContext())
                .append(":")
        .append(app.getApplicationPort())
//        serviceUrlBuilder.append("http://ec2-52-51-3-119.eu-west-1.compute.amazonaws.com:8080/")
//        serviceUrlBuilder.append("http://192.168.31.102:12769/")
//        serviceUrlBuilder.append("http://192.168.0.13:8080/")
//                .append(ApiCallService.REST_BASE_URL)
        ;
        serviceURL = serviceUrlBuilder.toString();
    }

    public static ApiCallService initializeService(){
        try {
            OkHttpClient httpClient = new OkHttpClient();
            httpClient.setReadTimeout(40, TimeUnit.SECONDS);
            httpClient.setConnectTimeout(40, TimeUnit.SECONDS);
            Gson gson = new GsonBuilder().setFieldNamingPolicy(
                    FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .excludeFieldsWithoutExposeAnnotation()
                    .registerTypeAdapter(Record.class, new Serializer<>(Record.class, null, false))
                        .create();

            httpClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    try{
                        Request request = chain.request();
                        Log.d("Request", request.urlString());
                        Request newRequest = request.newBuilder()
                                .header("Accept", "application/json")
                                .build();
                        Response originalResponse = chain.proceed(newRequest);
                        return originalResponse;
                    }
                    catch (IOException e){
                        return null;
                    }
                }
            });
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(getURL())
                    .client(httpClient)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson));
            Retrofit adapter = builder.build();
            apiCallService = adapter.create(ApiCallService.class);
            return apiCallService;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static ApiCallService getService(){
        if(apiCallService == null)
            return initializeService();
        return apiCallService;
    }

    public static String getURL(){
        return serviceURL;
    }

    public static ApiCallService getOBService() {
        return apiCallService;
    }

    public final static String getOB_SERVER(){
        return app.getServerFromContext();
    }

    public final static String getOB_URL(){
        return "http://"+getOB_SERVER()+":"+getOB_PORT()+"/openbravo";
    }

    public final static String getRestString(){
        return getOB_URL() + "/org.openbravo.service.json.jsonrest/";
    }

    public final static String getOB_PORT(){
        OB_PORT = String.valueOf(app.getApplicationPort());
        return OB_PORT;
    }

}
