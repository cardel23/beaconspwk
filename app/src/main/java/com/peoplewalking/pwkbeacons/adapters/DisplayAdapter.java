package com.peoplewalking.pwkbeacons.adapters;

import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.accent_systems.ibks_sdk.scanner.ASResultParser;
import com.accent_systems.ibks_sdk.utils.ASUtils;
import com.peoplewalking.pwkbeacons.R;
import com.peoplewalking.pwkbeacons.config.AppContext;
import com.peoplewalking.pwkbeacons.model.Beacon;
import com.peoplewalking.pwkbeacons.model.Container;
import com.peoplewalking.pwkbeacons.model.Location;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pwk04 on 05-19-17.
 */

public class DisplayAdapter extends ArrayAdapter<Container> {

    private List<Container> results;
    private static LayoutInflater mInflater;
    private AppContext app;

    public DisplayAdapter(Context context, @LayoutRes int resource, @NonNull List<Container> results) {
        super(context, resource, results);
        app = (AppContext) context.getApplicationContext();
        mInflater = LayoutInflater.from(context);
        if(results != null||results.size()>0)
            this.results = results;
        else this.results = new ArrayList<>();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public List<Container> getData(){
        return results;
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Container getItem(int position) {
        return results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_display, null);
        }
        TextView textView1, textView2;
        Container container = results.get(position);
        ScanResult result = container.getScanResult();
        Beacon beacon = container.getBeacon();
        StringBuilder builder = container.getStringBuilder();
        TextView frameType, rssi, field1, field2, field3, field4, field5, field6;
//        ImageView icon;
        frameType = (TextView) convertView.findViewById(R.id.textView12);
        rssi = (TextView) convertView.findViewById(R.id.textView9);
        field1 = (TextView) convertView.findViewById(R.id.textView5);
        field2 = (TextView) convertView.findViewById(R.id.textView6);
        field3 = (TextView) convertView.findViewById(R.id.textView7);
        field4 = (TextView) convertView.findViewById(R.id.textView8);
        field5 = (TextView) convertView.findViewById(R.id.textView10);
        field6 = (TextView) convertView.findViewById(R.id.textView);
//        icon = (ImageView) convertView.findViewById(R.id.imageView2);
        JSONObject advData = ASResultParser.getDataFromAdvertising(result);

        try {
            rssi.setText(String.valueOf(result.getRssi()));
//            if(!(positions.size() > 0 && !positions.get(position).equals(null) && !positions.get(position).equals(""))) {
//                positions.add("No se puede obtener la ubicación");
//                field6.setText(positions.get(position));
//            }
//            Location location = beacon.getLocation();

            try {
//                if(beacon != null){
                field6.setText("La ubicación es: " + beacon.getLocation().getName());
            }
             catch(Exception e){
                 try {
                    field6.setText(builder.toString());
                 } catch (Exception e1){
                     field6.setText("Ubicación desconocida");
                 }
            }
            switch (ASResultParser.getAdvertisingType(result)) {
                case ASUtils.TYPE_IBEACON:
                    frameType.setText("iBeacon");
                    field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                    field2.setText("UUID = " + advData.getString("UUID"));
                    field3.setText("Major = " + advData.getString("Major"));
                    field4.setText("Minor = " + advData.getString("Minor"));
                    field5.setVisibility(View.GONE);
//                    icon.setImageResource(R.drawable.ibeacon_filled50);
                    break;
                case ASUtils.TYPE_EDDYSTONE_UID:
                    frameType.setText("UID");
                    field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                    field2.setText("Namespace = "+advData.getString("Namespace"));
                    field3.setText("Instance = "+advData.getString("Instance"));
                    field4.setVisibility(View.GONE);
                    field5.setVisibility(View.GONE);
//                    icon.setImageResource(R.drawable.eddystone);
                    break;
                case ASUtils.TYPE_EDDYSTONE_URL:
                    frameType.setText("UID");
                    field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                    field2.setText("Url = "+advData.getString("Url"));
                    field3.setVisibility(View.GONE);
                    field4.setVisibility(View.GONE);
                    field5.setVisibility(View.GONE);
//                    icon.setImageResource(R.drawable.eddystone);
                    break;
                case ASUtils.TYPE_EDDYSTONE_TLM:
                    frameType.setText("TLM");
                    if(advData.getString("Version").equals("0")){
                        field1.setText("Version = "+advData.getString("Version"));
                        field2.setText("Temp = "+advData.getString("Temp"));
                        field3.setText("Vbatt = "+advData.getString("Vbatt"));
                        field4.setText("AdvCount = "+advData.getString("AdvCount"));
                        field5.setText("TimeUp = "+advData.getString("TimeUp"));
                    }
                    else{
                        field1.setText("Version = "+advData.getString("Version"));
                        field2.setText("EncryptedTLMData = "+advData.getString("EncryptedTLMData"));
                        field3.setText("Salt = "+advData.getString("Salt"));
                        field4.setText("AdvCount = "+advData.getString("AdvCount"));
                        field5.setText("IntegrityCheck = "+advData.getString("IntegrityCheck"));
                    }
//                    icon.setImageResource(R.drawable.eddystone);
                    break;
                case ASUtils.TYPE_EDDYSTONE_EID:
                    frameType.setText("EID");
                    field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                    field2.setText("EID = "+advData.getString("EID"));
                    field3.setVisibility(View.GONE);
                    field4.setVisibility(View.GONE);
                    field5.setVisibility(View.GONE);
//                    icon.setImageResource(R.drawable.eddystone);
                    break;
                default:
                    frameType.setText("Bluetooth");
                    if(!advData.getString("AdvTxPower").equals(null))
                        field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                    else
                        field1.setVisibility(View.GONE);
                    field2.setVisibility(View.GONE);
                    field3.setVisibility(View.GONE);
                    field4.setVisibility(View.GONE);
                    field5.setVisibility(View.GONE);
//                    icon.setImageResource(R.drawable.bluetooth50);
                    break;
            }
            Log.i("SWITCH","TODO COOL");
        }
        catch (JSONException e){
            frameType.setText("Bluetooth");
            field1.setText("Nombre: " + result.getDevice().getName());
            field2.setText(result.getDevice().getAddress());
            field3.setText("Tipo de dispositivo: desconocido");
            field4.setVisibility(View.GONE);
            field5.setVisibility(View.GONE);
//            icon.setImageResource(R.drawable.bluetooth50);
            Log.i(e.getClass().getSimpleName(),"NO SIRVIO LA MIERDA");
            Log.e("ADAPTER",e.getMessage(),e);
        }
        catch (IndexOutOfBoundsException e){
            return null;
        }
        return convertView;
    }

    @Override
    public int getPosition(Container item) {
        return results.indexOf(item);
    }

    public void updateItem(int position, ScanResult result){

    }

//    public boolean containsElement(int position, ScanResult result){
//        try {
//            if (getItem(position).getScanResult().getDevice().getAddress().contains(result.getDevice().getAddress())) {
//                return true;
//            } else
//                return false;
//        }
//        catch (Exception e) {
//            Log.wtf("WTF","MIERDA",e);
//            return false;
//        }
//    }
//    public boolean containsElement(int position, String string){
//        try {
//            if (positions.get(position).contains(string)) {
//                return true;
//            } else
//                return false;
//        }
//        catch (Exception e) {
//            Log.wtf("WTF","MIERDA",e);
//            return false;
//        }
//    }

//    public void setResult(int position, ScanResult result, StringBuilder sb){
//        try {
//            if(containsElement(position,sb.toString()))
//                positions.set(position, sb.toString());
//            else
//                positions.add(sb.toString());
//        }
//        catch (IndexOutOfBoundsException | NullPointerException e){
//            positions.add(position, sb.toString());
//        }
//        if(containsElement(position,result))
//            getData().set(position,result);
//        else
//            getData().add(result);
//
//    }
//
//    public void addResult(ScanResult result, StringBuilder sb){
//            positions.add(sb.toString());
//            getData().add(result);
//    }

}
