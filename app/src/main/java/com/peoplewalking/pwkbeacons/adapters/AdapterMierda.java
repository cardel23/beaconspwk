package com.peoplewalking.pwkbeacons.adapters;

import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.accent_systems.ibks_sdk.scanner.ASResultParser;
import com.accent_systems.ibks_sdk.utils.ASUtils;
import com.peoplewalking.pwkbeacons.R;
import com.peoplewalking.pwkbeacons.config.AppContext;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by pwk04 on 05-23-17.
 */

public class AdapterMierda extends ArrayAdapter<ScanResult> {
    private static LayoutInflater mInflater;
    private int resource;

    private List<ScanResult> results;
//    private static LayoutInflater mInflater;
    private AppContext app;
    TextView frameType, rssi, field1, field2, field3, field4, field5;
    ImageView icon;

    public AdapterMierda(Context context, @LayoutRes int resource, @NonNull List<ScanResult> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.results = objects;
    }


//    @Override
//    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        return super.getView(position, convertView, parent);
//    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                mInflater = LayoutInflater.from(getContext());
                convertView = mInflater.inflate(resource, parent, false);
            }
            ScanResult result = results.get(position);

            frameType = (TextView) convertView.findViewById(R.id.textView12);
            rssi = (TextView) convertView.findViewById(R.id.textView9);
            field1 = (TextView) convertView.findViewById(R.id.textView5);
            field2 = (TextView) convertView.findViewById(R.id.textView6);
            field3 = (TextView) convertView.findViewById(R.id.textView7);
            field4 = (TextView) convertView.findViewById(R.id.textView8);
            field5 = (TextView) convertView.findViewById(R.id.textView10);
            icon = (ImageView) convertView.findViewById(R.id.imageView2);
            JSONObject advData = ASResultParser.getDataFromAdvertising(result);

            try {
                rssi.setText(Integer.toString(result.getRssi()));
                switch (ASResultParser.getAdvertisingType(result)) {
                    case ASUtils.TYPE_IBEACON:
                        frameType.setText("iBeacon");
                        field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                        field2.setText("UUID = " + advData.getString("UUID"));
                        field3.setText("Major = " + advData.getString("Major"));
                        field4.setText("Minor = " + advData.getString("Minor"));
                        field5.setVisibility(View.GONE);
                        icon.setImageResource(R.drawable.ibeacon_filled50);
                        break;
                    case ASUtils.TYPE_EDDYSTONE_UID:
                        frameType.setText("UID");
                        field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                        field2.setText("Namespace = "+advData.getString("Namespace"));
                        field3.setText("Instance = "+advData.getString("Instance"));
                        field4.setVisibility(View.GONE);
                        field5.setVisibility(View.GONE);
                        icon.setImageResource(R.drawable.eddystone);
                        break;
                    case ASUtils.TYPE_EDDYSTONE_URL:
                        frameType.setText("UID");
                        field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                        field2.setText("Url = "+advData.getString("Url"));
                        field3.setVisibility(View.GONE);
                        field4.setVisibility(View.GONE);
                        field5.setVisibility(View.GONE);
                        icon.setImageResource(R.drawable.eddystone);
                        break;
                    case ASUtils.TYPE_EDDYSTONE_TLM:
                        frameType.setText("TLM");
                        if(advData.getString("Version").equals("0")){
                            field1.setText("Version = "+advData.getString("Version"));
                            field2.setText("Temp = "+advData.getString("Temp"));
                            field3.setText("Vbatt = "+advData.getString("Vbatt"));
                            field4.setText("AdvCount = "+advData.getString("AdvCount"));
                            field5.setText("TimeUp = "+advData.getString("TimeUp"));
                        }
                        else{
                            field1.setText("Version = "+advData.getString("Version"));
                            field2.setText("EncryptedTLMData = "+advData.getString("EncryptedTLMData"));
                            field3.setText("Salt = "+advData.getString("Salt"));
                            field4.setText("AdvCount = "+advData.getString("AdvCount"));
                            field5.setText("IntegrityCheck = "+advData.getString("IntegrityCheck"));
                        }
                        icon.setImageResource(R.drawable.eddystone);
                        break;
                    case ASUtils.TYPE_EDDYSTONE_EID:
                        frameType.setText("EID");
                        field1.setText("AdvTxPower = " + advData.getString("AdvTxPower"));
                        field2.setText("EID = "+advData.getString("EID"));
                        field3.setVisibility(View.GONE);
                        field4.setVisibility(View.GONE);
                        field5.setVisibility(View.GONE);
                        icon.setImageResource(R.drawable.eddystone);
                        break;
                    default:
                        frameType.setText("Bluetooth");
                        field1.setVisibility(View.GONE);
                        field2.setVisibility(View.GONE);
                        field3.setVisibility(View.GONE);
                        field4.setVisibility(View.GONE);
                        field5.setVisibility(View.GONE);
                        icon.setImageResource(R.drawable.bluetooth50);
                        break;
                }
        }
        catch (Exception e){
            frameType.setText("Bluetooth");
            field1.setVisibility(View.GONE);
            field2.setVisibility(View.GONE);
            field3.setVisibility(View.GONE);
            field4.setVisibility(View.GONE);
            field5.setVisibility(View.GONE);
            icon.setImageResource(R.drawable.bluetooth50);
            e.printStackTrace();
        }
        Log.e("ADAPTER", "ESTA MIERDA ESTA FUNCIONANDO");
        return convertView;
    }
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public List<ScanResult> getData(){
        return results;
    }

    @Override
    public int getCount() {
        return getData().size();
    }

    @Override
    public ScanResult getItem(int position) {
        return getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public int getPosition(ScanResult item) {
        return getData().indexOf(item);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return super.areAllItemsEnabled();
    }

    @Override
    public boolean isEnabled(int position) {
        return super.isEnabled(position);
    }
}
