package com.peoplewalking.pwkbeacons;

import android.Manifest;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


import com.accent_systems.ibks_sdk.EDSTService.ASEDSTCallback;
import com.accent_systems.ibks_sdk.EDSTService.ASEDSTService;
import com.accent_systems.ibks_sdk.EDSTService.ASEDSTSlot;
import com.accent_systems.ibks_sdk.GlobalService.ASGlobalCallback;
import com.accent_systems.ibks_sdk.GlobalService.ASGlobalDefs;
import com.accent_systems.ibks_sdk.connections.ASConDeviceCallback;
import com.accent_systems.ibks_sdk.iBeaconService.ASiBeaconCallback;
import com.accent_systems.ibks_sdk.iBeaconService.ASiBeaconSlot;
import com.accent_systems.ibks_sdk.scanner.ASBleScanner;
import com.accent_systems.ibks_sdk.scanner.ASResultParser;
import com.accent_systems.ibks_sdk.scanner.ASScannerCallback;
import com.accent_systems.ibks_sdk.utils.ASUtils;
import com.accent_systems.ibks_sdk.utils.AuthorizedServiceTask;
import com.google.android.gms.common.AccountPicker;
import com.google.sample.libproximitybeacon.ProximityBeacon;
import com.google.sample.libproximitybeacon.ProximityBeaconImpl;
import com.peoplewalking.pwkbeacons.adapters.DisplayAdapter;
import com.peoplewalking.pwkbeacons.api.ApiCallServiceClient;
import com.peoplewalking.pwkbeacons.config.AppContext;
import com.peoplewalking.pwkbeacons.listeners.OnPostExecuteListener;
import com.peoplewalking.pwkbeacons.model.AppUser;
import com.peoplewalking.pwkbeacons.model.Beacon;
import com.peoplewalking.pwkbeacons.model.Container;
import com.peoplewalking.pwkbeacons.model.Record;
import com.peoplewalking.pwkbeacons.tasks.Task;
import com.peoplewalking.pwkbeacons.tasks.UserTask;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements ASScannerCallback, ASConDeviceCallback, ASEDSTCallback, ASiBeaconCallback, ASGlobalCallback, OnPostExecuteListener<Beacon> {

    public static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    public static final int SCOPE_USERLOCATION    =  0;
    public static final int SCOPE_CLOUDPLATFORM   =  1;

//    private List<String> scannedDevicesList;
    private List<Container> scannedDevicesList;
//    private ArrayAdapter<String> adapter;
    private DisplayAdapter adapter;
//    private AdapterMierda adapter;
    private AppContext app;


    BluetoothGattCharacteristic myCharRead;
    BluetoothGattCharacteristic myCharWrite;
    //DEFINE LAYOUT
    ListView devicesList;

    public static ProximityBeacon client;
    SharedPreferences getPrefs;
    public static Activity actv;

    static ProgressDialog connDialog;
    private static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = (AppContext) getApplicationContext();

//        app.setService(new ApiCallServiceClient(app).initializeService());
        actv = this;
        //Define listview in layout
        devicesList = (ListView) findViewById(R.id.devicesList);
        //Setup list on device click listener
        setupListClickListener();

        //Inicialize de devices list
        scannedDevicesList = new ArrayList<>();

        //Inicialize the list adapter for the listview with params: Context / Layout file / TextView ID in layout file / Devices list
//        adapter = new AdapterMierda(this, R.layout.mierda_adapter, scannedDevicesList);
        adapter = new DisplayAdapter(this, R.layout.adapter_display, scannedDevicesList);
//        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, scannedDevicesList);
        //Set the adapter to the listview
        devicesList.setAdapter(adapter);


//        prepareApp();

//        connDialog = new ProgressDialog(MainActivity.this);
//        connDialog.setTitle("Please wait...");

        //  checkBlePermissions();
//        startScan();

//        getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//
//        if(getPrefs.getString("clientName", null)!=null){
//
//            try{
//                client = new ProximityBeaconImpl(MainActivity.this, getPrefs.getString("clientName", "poppy-coda-client"));
//                new AuthorizedServiceTask(MainActivity.this, getPrefs.getString("clientName", "poppy-coda-client"),SCOPE_USERLOCATION).execute();
//                new AuthorizedServiceTask(MainActivity.this, getPrefs.getString("clientName", "poppy-coda-client"),SCOPE_CLOUDPLATFORM).execute();
//                getProjectList();
//
//            } catch (final Exception ee) {
//                Log.i(TAG,"CLIENT ERROR: " + ee.toString());
//                pickUserAccount();
//            }
//        }else{
//            pickUserAccount();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        prepareApp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
//		v =  (ProgressBar) MenuItemCompat.getActionView(miActionProgressItem);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepareApp(){
        final EditText txtName = new EditText(this);

        // Set the default text to a link of the Queen
//        txtName.setHint("http://www.librarising.com/astrology/celebs/images2/QR/queenelizabethii.jpg");
        if (!app.isServerSet()){
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        else {
            app.setService(new ApiCallServiceClient(app).initializeService());

            Iterator<AppUser> iterator = AppUser.findAll(AppUser.class);
            if (iterator.hasNext()) {
                AppUser user = iterator.next();
                Toast.makeText(this, "Bienvenido de nuevo " + user.getName(), Toast.LENGTH_LONG).show();
                startScan();
            } else {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
                        .setTitle("Crear usuario")
                        .setMessage("Escriba su nombre de usuario")
                        .setView(txtName)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String name = txtName.getText().toString();
                                UserTask userTask = new UserTask(MainActivity.this, name);
                                userTask.setCallback(new OnPostExecuteListener<AppUser>() {
                                    @Override
                                    public void onPostExecute(AppUser response, List<String> errorMessages) {

                                    }

                                    @Override
                                    public void onPostExecute(AppUser appUser, retrofit.Response response, StringBuilder sb, ScanResult result) {
                                        if (appUser != null) {
                                            startScan();
                                        }
                                    }

                                    @Override
                                    public void onPostExecute(AppUser appUser) {

                                    }
                                });
                                dialog.dismiss();
                                userTask.execute();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                prepareApp();
                            }
                        });
                dialogBuilder.show();
            }
        }
    }
    private void pickUserAccount() {
        Log.i(TAG, "PICK USER - CALLED");
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(
                null, null, accountTypes, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == 1 && resultCode == Activity.RESULT_CANCELED) {
            return;
        }else{
            if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
                if (resultCode == Activity.RESULT_OK) {
                    String name = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    try{
                        client = new ProximityBeaconImpl(MainActivity.this, name);

                        new AuthorizedServiceTask(MainActivity.this, name,SCOPE_USERLOCATION).execute();
                        new AuthorizedServiceTask(MainActivity.this, name,SCOPE_CLOUDPLATFORM).execute();
                        PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().putString("clientName", name).commit();
                        getProjectList();
                    } catch (final Exception ee) {
                        Log.i(TAG,"CLIENT ERROR: "+ ee.toString());
                    }

                }else{
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static void getProjectList()
    {
        Callback listProjectsCallback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.d(TAG, "listProjectsCallback - Failed request: " + request, e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String body = response.body().string();
                if (response.isSuccessful()) {

                    try {
                        JSONObject json = new JSONObject(body);
                        final JSONArray projects = json.getJSONArray("projects");
                        final int numprojects = projects.length();
                        final String [] items = new String[numprojects+1];
                        for(int i=0;i<numprojects;i++)
                        {
                            items[i] =  projects.getJSONObject(i).getString("name");
                        }
                        items[numprojects] = "None";
                        AlertDialog.Builder builder=new AlertDialog.Builder(actv);
                        builder.setTitle("Select a project");
                        builder.setCancelable(false);
                        builder.setItems(items, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {

                                    if(which == numprojects)
                                    {
                                        PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectId", "null").commit();
                                        PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectName", "null").commit();
                                    }
                                    else {
                                        PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectId", projects.getJSONObject(which).getString("projectId")).commit();
                                        PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectName", projects.getJSONObject(which).getString("name")).commit();
                                        String projectId = PreferenceManager.getDefaultSharedPreferences(actv).getString("projectId", "null");
                                        String projectName = PreferenceManager.getDefaultSharedPreferences(actv).getString("projectName", "null");
                                        Log.i(TAG,"Project selected: "+ projectId);
                                    }
                                } catch (JSONException e) {
                                    Log.e(TAG, "listProjectsCallback - JSONException", e);
                                }
                            }
                        });

                        builder.show();

                    } catch (JSONException e) {
                        PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectId", "null").commit();
                        PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectName", "null").commit();
                        Log.i(TAG, "listProjectsCallback - This account has no an associated project");
                    }
                } else {
                    PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectId", "null").commit();
                    PreferenceManager.getDefaultSharedPreferences(actv).edit().putString("projectName", "null").commit();
                    Log.d(TAG, "Unsuccessful project list request: " + body);
                }
            }
        };

        client.getProjectList(listProjectsCallback);
    }

    void setupListClickListener(){
        devicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent(MainActivity.this, LocationMapActivity.class);
//                startActivity(intent);
            }
        });
    }

    private void startScan(){
        int err;
        new ASBleScanner(this, this).setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);
        err = ASBleScanner.startScan();
        if(err != ASUtils.TASK_OK) {
            Log.i(TAG, "startScan - Error (" + Integer.toString(err) + ")");

            if(err == ASUtils.ERROR_LOCATION_PERMISSION_NOT_GRANTED){
                requestLocationPermissions();
            }
        }
    }

    @TargetApi(23)
    public void requestLocationPermissions(){
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "LOCATION PERMISSION GRANTED");
                    startScan();
                } else {
                    Log.i(TAG, "LOCATION PERMISSION NOT GRANTED");
                }
                return;
            }
        }
    }

    @Override
    public void onPostExecute(Beacon aBeacon, retrofit.Response response, StringBuilder dataBuilder, ScanResult result) {
        boolean contains = false;
        Log.i("LIST SIZE ",String.valueOf(scannedDevicesList.size()));

        for(int i = 0; i< scannedDevicesList.size(); i++){
            if(scannedDevicesList.get(i).getScanResult().getDevice().getAddress().contains(result.getDevice().getAddress())) {
                scannedDevicesList.set(i, new Container(result, aBeacon, dataBuilder));
                contains = true;
                break;
            }
        }

        if(!contains){
            scannedDevicesList.add(new Container(result,aBeacon,dataBuilder));
        }

        //After modify the list, notify the adapter that changes have been made so it updates the UI.
        //UI changes must be done in the main thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("LIST SIZE BEFORE NOTIFY",String.valueOf(adapter.getData().size()));
                adapter.notifyDataSetChanged();
                Log.i("LIST SIZE AFTER NOTIFY",String.valueOf(adapter.getData().size()));
            }
        });

//        Log.i(TAG,"Llamada onPostExecute "+response.message());

    }

    @Override
    public void onPostExecute(Beacon aBeacon) {

    }

    @Override
    public void scannedBleDevices(ScanResult result){
        displayBeacons(result);
    }

    private void displayBeacons(ScanResult result){

        JSONObject advData = ASResultParser.getDataFromAdvertising(result);
        Record record = new Record();
        record.setIsactive("Y");
//        record.setBeaconId(UUID.nameUUIDFromBytes(result.getScanRecord().getBytes()).toString());
        record.setMacAddress(result.getDevice().getAddress());
        record.setRssi(result.getRssi());
        try {
            if(advData.getString("AdvTxPower") != null)
                record.setAdvTx(Integer.parseInt(advData.getString("AdvTxPower")));
            else
                record.setAdvTx(0);
            if(advData.getString("FrameType") != null)
                record.setFrameType(advData.getString("FrameType"));
            else
                record.setFrameType(String.valueOf(ASUtils.TYPE_UNKNOWN));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            if(advData.getString("FrameType") != null)
                record.setFrameType(advData.getString("FrameType"));
            else
                record.setFrameType(String.valueOf(ASUtils.TYPE_UNKNOWN));
        } catch (JSONException e) {
            e.printStackTrace();
            record.setFrameType(String.valueOf(ASUtils.TYPE_UNKNOWN));
        }
        Task task = new Task(record,this,result);
        task.execute();
    }

    private StringBuilder displayBeacon(ScanResult result, String location){
        StringBuilder dataBuilder = new StringBuilder();
        JSONObject advData = ASResultParser.getDataFromAdvertising(result);
        dataBuilder.append("RSSI:"+result.getRssi());
        dataBuilder.append("\n");
        dataBuilder.append("NOMBRE DISPOSITIVO:"+result.getDevice().getName());
        dataBuilder.append("\n");
        dataBuilder.append("DIRECCION MAC("+result.getDevice().getAddress()+")");
        dataBuilder.append("\n");
        switch (ASResultParser.getAdvertisingType(result)){
            case ASUtils.TYPE_IBEACON:
                try {
                    dataBuilder.append("FrameType = iBeacon");
                    dataBuilder.append("\n");
                    dataBuilder.append("AdvTxPower = " + advData.getString("AdvTxPower"));
                    dataBuilder.append("\n");
                    dataBuilder.append("UUID = " + advData.getString("UUID"));
                    dataBuilder.append("\n");
                    dataBuilder.append("Major = " + advData.getString("Major"));
                    dataBuilder.append("\n");
                    dataBuilder.append("Minor = " + advData.getString("Minor"));
                    dataBuilder.append("\n");
                    Log.i(TAG,advData.toString());
                }catch (JSONException ex){
                    Log.i(TAG,"Error parsing JSON",ex);
                }
                break;
            case ASUtils.TYPE_EDDYSTONE_UID:
                try {
                    dataBuilder.append("FrameType = eddystone-UID");
                    dataBuilder.append("\n");
                    dataBuilder.append("AdvTxPower = " + advData.getString("AdvTxPower"));
                    dataBuilder.append("\n");
                    dataBuilder.append("Namespace = "+advData.getString("Namespace"));
                    dataBuilder.append("\n");
                    dataBuilder.append("Instance = "+advData.getString("Instance"));
                    dataBuilder.append("\n");
                    Log.i(TAG,advData.toString());
                }catch (JSONException ex){
                    Log.i(TAG,"Error parsing JSON",ex);
                }
                break;
            case ASUtils.TYPE_EDDYSTONE_URL:
                try {
                    dataBuilder.append("FrameType = eddystone-URL");
                    dataBuilder.append("\n");
                    dataBuilder.append("AdvTxPower = " + advData.getString("AdvTxPower"));
                    dataBuilder.append("\n");
                    dataBuilder.append("Url = "+advData.getString("Url"));
                    dataBuilder.append("\n");
                    Log.i(TAG,advData.toString());
                }catch (JSONException ex){
                    Log.i(TAG,"Error parsing JSON",ex);
                }
                break;
            case ASUtils.TYPE_EDDYSTONE_TLM:
                try {
                    if(advData.getString("Version").equals("0")){
                        dataBuilder.append("FrameType = eddystone-TLM");
                        dataBuilder.append("\n");
                        dataBuilder.append("Version = "+advData.getString("Version"));
                        dataBuilder.append("\n");
                        dataBuilder.append("Temp = "+advData.getString("Temp"));
                        dataBuilder.append("\n");
                        dataBuilder.append("Vbatt = "+advData.getString("Vbatt"));
                        dataBuilder.append("\n");
                        dataBuilder.append("AdvCount = "+advData.getString("AdvCount"));
                        dataBuilder.append("\n");
                        dataBuilder.append("TimeUp = "+advData.getString("TimeUp"));
                        dataBuilder.append("\n");
                    }
                    else{
                        dataBuilder.append("FrameType = eddystone-TLM");
                        dataBuilder.append("\n");
                        dataBuilder.append("Version = "+advData.getString("Version"));
                        dataBuilder.append("\n");
                        dataBuilder.append("EncryptedTLMData = "+advData.getString("EncryptedTLMData"));
                        dataBuilder.append("\n");
                        dataBuilder.append("Salt = "+advData.getString("Salt"));
                        dataBuilder.append("\n");
                        dataBuilder.append("IntegrityCheck = "+advData.getString("IntegrityCheck"));
                        dataBuilder.append("\n");
                    }
                    Log.i(TAG,advData.toString());
                }catch (JSONException ex){
                    Log.i(TAG,"Error parsing JSON",ex);
                }
                break;
            case ASUtils.TYPE_EDDYSTONE_EID:
                new ASEDSTService(null,this,10);
                ASEDSTService.setClient_ProjectId(client, getPrefs.getString("projectId", null));
                ASEDSTService.getEIDInClearByTheAir(result);
                try {
                    dataBuilder.append("FrameType = eddystone-EID");
                    dataBuilder.append("\n");

                    dataBuilder.append("AdvTxPower = " + advData.getString("AdvTxPower"));
                    dataBuilder.append("\n");
                    dataBuilder.append("EID = "+advData.getString("EID"));
                    dataBuilder.append("\n");
                    Log.i(TAG,advData.toString());
                }catch (JSONException ex){
                    Log.i(TAG,"Error parsing JSON",ex);
                }
                break;
            case ASUtils.TYPE_DEVICE_CONNECTABLE:
                dataBuilder.append("DEVICE = " + result.getDevice().getName()+" - CONNECTABLE");
                dataBuilder.append("\n");
                try {

                    dataBuilder.append("AdvTxPower = " + advData.getString("AdvTxPower"));
                    dataBuilder.append("\n");
                } catch (JSONException e) {
//                    e.printStackTrace();
                }
                dataBuilder.append("advData = " + advData.toString());
                dataBuilder.append("\n");
                Log.i(TAG,advData.toString());
                break;
            case ASUtils.TYPE_UNKNOWN:
                dataBuilder.append("DEVICE = " + result.getDevice().getName()+" - UNKNOWN");
                dataBuilder.append("\n");
                try {
                    dataBuilder.append("AdvTxPower = " + advData.getString("AdvTxPower"));
                    dataBuilder.append("\n");
                } catch (JSONException e) {
//                    e.printStackTrace();
                }
                dataBuilder.append("advData = " + advData.toString());
                dataBuilder.append("\n");
                Log.i(TAG,advData.toString());
                break;
            default:
                break;
        }
        dataBuilder.append(location);
        return dataBuilder;
    }

    @Override
    //implementation of ASConDeviceCallback
    public void onChangeStatusConnection(int result, BluetoothGatt blgatt){
        switch (result){
            case ASUtils.GATT_DEV_CONNECTED:
                Log.i(TAG,"onChangeStatusConnection - DEVICE CONNECTED: "+blgatt.getDevice().getName());
                break;
            case ASUtils.GATT_DEV_DISCONNECTED:
                Log.i(TAG,"onChangeStatusConnection - DEVICE DISCONNECTED: "+blgatt.getDevice().getName());
                if (connDialog != null && connDialog.isShowing()) {
                    connDialog.dismiss();
                }
                break;
            default:
                Log.i(TAG,"onChangeStatusConnection - ERROR PARSING");
                break;
        }

    }

    //implementation of ASConDeviceCallback
    public void onServicesCharDiscovered(int result, BluetoothGatt blgatt, ArrayList<BluetoothGattService> services, ArrayList<BluetoothGattCharacteristic> characteristics)
    {
        switch (result){
            case ASUtils.GATT_SERV_DISCOVERED_OK:
                int err;
                Log.i(TAG, "onServicesCharDiscovered - SERVICES DISCOVERED OK: "+blgatt.getDevice().getName());

                /**** Example to read a characteristic ***
                 myCharRead = ASConDevice.findCharacteristic("00002a28");
                 ASConDevice.readCharacteristic(myCharRead);
                 /*****************************************/


                /**** Example to set Eddystone Slots a characteristic ***

                 ASEDSTSlot[] slots = new ASEDSTSlot[4];
                 ASEDSTService.setClient_ProjectId(client,getPrefs.getString("projectId", null));

                 slots[0] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_UID,800,-4,-35,"0102030405060708090a0b0c0d0e0f11");
                 slots[1] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_EID,950,-4,-35,"1112131415161718191a1b1c1d1e1f200a");
                 slots[2] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_URL,650,0,-21,"http://goo.gl/yb6Mgt");
                 slots[3] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_TLM,60000,4,-17,null);
                 ASEDSTService.setEDSTSlots(slots);
                 /********************************************************/

                /**** Example to set iBeacon Slots a characteristic ***
                 ASiBeaconSlot[] slotsib = new ASiBeaconSlot[2];
                 slotsib[0] = new ASiBeaconSlot(false,800,0,-21,"01010101010101010101010101010101","0002","0003",false);
                 slotsib[1] = new ASiBeaconSlot(false,400,-8,-40,"01010101010101010101010101010102","0004","0005",true);
                 ASiBeaconService.setiBeaconSlots(slotsib);
                 /******************************************************/


                /*** Example to get EDST or iBeacon Slots ***/
                // ASEDSTService.setClient_ProjectId(client,getPrefs.getString("projectId", null));
                ASEDSTService.getEDSTSlots();
                //ASiBeaconService.getiBeaconSlots();
                /********************************************/

                /*** Example to set Characteristics ***
                 ASEDSTService.setActiveSlot(2);
                 //ASEDSTService.setRadioTxPower(-4);
                 //ASiBeaconService.setExtraByte(true);
                 //ASiBeaconService.setUUIDMajorMinor(false,"0102030405060708090a0b0c0d0e0f10","0001","0002");
                 //ASGlobalService.setONOFFAdvertising(9,22);
                 //ASGlobalService.setDeviceName("iBKS-TEST");
                 /*****************************************************/

                /*** Example to get Characteristics ***
                 ASEDSTService.getLockState();
                 //ASiBeaconService.getActiveSlot();
                 //ASGlobalService.getDeviceName();
                 /*****************************************************/

                break;
            case ASUtils.GATT_SERV_DISCOVERED_ERROR:
                Log.i(TAG, "onServicesCharDiscovered - SERVICES DISCOVERED ERROR: "+blgatt.getDevice().getName());
                break;
            default:
                Log.i(TAG, "onServicesCharDiscovered - ERROR PARSING");
                break;
        }
    }

    //implementation of ASConDeviceCallback
    public void onReadDeviceValues(int result, BluetoothGattCharacteristic characteristic, String value){
        switch (result){
            case ASUtils.GATT_READ_SUCCESSFULL:
                Log.i(TAG, "onReadDeviceValues - READ VALUE: " + value);
                break;
            case ASUtils.GATT_READ_ERROR:
                Log.i(TAG, "onReadDeviceValues - READ ERROR");
                break;
            case ASUtils.GATT_NOTIFICATION_RCV:
                Log.i(TAG, "onReadDeviceValues - READ NOTIFICATION: " + value);
                break;
            case ASUtils.GATT_RSSI_OK:
                Log.i(TAG, "onReadDeviceValues - READ RSSI: " + value);
                break;
            case ASUtils.GATT_RSSI_ERROR:
                Log.i(TAG, "onReadDeviceValues - READ RSSI ERROR");
                break;
            default:
                Log.i(TAG, "onReadDeviceValues - ERROR PARSING");
                break;
        }
    }

    //implementation of ASConDeviceCallback
    public void onWriteDeviceChar(int result, BluetoothGattCharacteristic characteristic) {
        switch (result) {
            case ASUtils.GATT_WRITE_SUCCESSFULL:
                Log.i(TAG, "onWriteDeviceChar - WRITE SUCCESSFULL on: " + characteristic.getUuid().toString() );
                break;
            case ASUtils.GATT_WRITE_ERROR:
                Log.i(TAG, "onWriteDeviceChar - WRITE ERROR on: " + characteristic.getUuid().toString() );
                break;
            default:
                Log.i(TAG, "onWriteDeviceChar - ERROR PARSING");
                break;
        }
    }

    //implementation of ASEDSTCallback
    public void onReadEDSTCharacteristic(int result, BluetoothGattCharacteristic characteristic, byte[] readval)
    {
        Log.i(TAG,"onReadEDSTCharacteristic - result = " + result + " characteristic = " + characteristic.getUuid() +" readval = " + ASResultParser.byteArrayToHex(readval));
    }
    //implementation of ASEDSTCallback
    public void onWriteEDSTCharacteristic(int result, BluetoothGattCharacteristic characteristic)
    {
        Log.i(TAG,"onWriteEDSTCharacteristic - result = " + result /*+ " characteristic = " + characteristic.getUuid()*/ );
    }
    //implementation of ASEDSTCallback
    public void onEDSTSlotsWrite(int result)
    {
        if(result == ASUtils.WRITE_OK) {
            Log.i(TAG, "onEDSTSlotsWrite - Write OK!");
        }
        else
            Log.i(TAG,"onEDSTSlotsWrite - Error (" + Integer.toString(result) + ")");

    }
    //implementation of ASEDSTCallback
    public void onGetEDSTSlots(int result, ASEDSTSlot[] slots){
        if(result == ASUtils.READ_OK)
        {
            /**** Reading EID In Clear (if there's a slot configured as EID) ****
             for(int i=0;i<slots.length;i++) {
             if(slots[i].frame_type == ASEDSTDefs.FT_EDDYSTONE_EID) {
             ASEDSTService.setClient_ProjectId(client, getPrefs.getString("projectId", null));
             ASEDSTService.getEIDInClear(i);
             }
             }
             /********************************************************************/
            for(int i=0;i<slots.length;i++){
                Log.i(TAG,"onGetEDSTSlots - slot "+i+" advint = "+ Integer.toString(slots[i].adv_int)+ " txpower = "+ slots[i].tx_power + " advtxpower = "+ slots[i].adv_tx_power +" frame type = 0x"+ Integer.toHexString(slots[i].frame_type)+" result = "+ slots[i].data );
            }

        }
        else
            Log.i(TAG,"onGetEDSTSlots - Error (" + Integer.toString(result) + ")");

        //Close dialog
        if (connDialog != null && connDialog.isShowing()) {
            connDialog.dismiss();
        }
    }
    //implementation of ASEDSTCallback
    public void onGetEIDInClear(int result, String EID, String msg){
        if(result == ASUtils.READ_OK) {
            Log.i(TAG, "onGetEIDInClear - EID read OK = "+ EID);
        }
        else
            Log.i(TAG,"onGetEIDInClear - Error reading EID (" + Integer.toString(result) + "): "+ msg);

    }


    //implementation of ASiBeaconCallback
    public void onReadiBeaconCharacteristic(int result, BluetoothGattCharacteristic characteristic, byte[] readval)
    {
        Log.i(TAG,"onReadiBeaconCharacteristic - result = " + result/* + " characteristic = " + characteristic.getUuid() +" readval = " + ASResultParser.byteArrayToHex(readval)*/);
    }
    public void onWriteiBeaconCharacteristic(int result, BluetoothGattCharacteristic characteristic)
    {
        Log.i(TAG,"onWriteiBeaconCharacteristic - result = " + result /*+ " characteristic = " + characteristic.getUuid()*/ );
    }

    //implementation of ASiBeaconCallback
    public void oniBeaconSlotsWrite(int result)
    {
        if(result == ASUtils.WRITE_OK) {
            Log.i(TAG, "oniBeaconSlotsWrite - Write OK!");
        }
        else
            Log.i(TAG,"oniBeaconSlotsWrite - Error (" + Integer.toString(result) + ")");

    }
    //implementation of ASiBeaconCallback
    public void onGetiBeaconSlots(int result, ASiBeaconSlot[] slots){
        if(result == ASUtils.READ_OK)
        {
            for(int i=0;i<slots.length;i++){
                Log.i(TAG,"onGetiBeaconSlots - slot "+i+" clear slot = "+ slots[i].clearslot+" advint = "+ Integer.toString(slots[i].adv_int)+" txpower = "+ slots[i].tx_power+" advtxpower = "+ slots[i].adv_tx_power+" uuid = "+ slots[i].UUID+" major = "+ slots[i].Major+" minor = "+ slots[i].Minor+" extra byte = "+ slots[i].ExtraByte);
            }
        }
        else
            Log.i(TAG,"onGetiBeaconSlots - Error (" + Integer.toString(result) + ")");
    }

    //implementation of ASGlobalCallback
    public void onReadGlobalCharacteristic(int result, BluetoothGattCharacteristic characteristic, byte[] readval)
    {
        if(result == ASUtils.READ_OK) {
            Log.i(TAG, "onReadGlobalCharacteristic - read OK!");
            if(characteristic.getUuid().toString().contains(ASGlobalDefs.DEVICE_NAME)){
                Log.i(TAG,"Device Name is "+ASResultParser.StringHexToAscii(ASResultParser.byteArrayToHex(readval)));
            }
        }
        else
            Log.i(TAG,"onReadGlobalCharacteristic - Error (" + Integer.toString(result) + ")");
    }
    //implementation of ASGlobalCallback
    public void onWriteGlobalCharacteristic(int result, BluetoothGattCharacteristic characteristic)
    {
        if(result == ASUtils.WRITE_OK) {
            Log.i(TAG, "onWriteGlobalCharacteristic - Write OK!");
        }
        else
            Log.i(TAG,"onWriteGlobalCharacteristic - Error (" + Integer.toString(result) + ")");
    }

    @Override
    public void onPostExecute(Beacon response, List<String> errorMessages) {

    }
}
