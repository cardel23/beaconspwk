package com.peoplewalking.pwkbeacons.config;

import android.app.Application;
import android.bluetooth.le.ScanResult;
import android.support.v7.app.AppCompatActivity;

import com.orm.SugarApp;
import com.peoplewalking.pwkbeacons.MainActivity;
import com.peoplewalking.pwkbeacons.api.ApiCallService;
import com.peoplewalking.pwkbeacons.api.ApiCallServiceClient;
import com.peoplewalking.walkingdroid.util.WDApp;

import java.util.List;

/**
 * Created by pwk04 on 05-17-17.
 */

public class AppContext extends WDApp {

    private ApiCallService service;
    private List<ScanResult> results;

    public ApiCallService getService() {
        return service;
    }

    public void setService(ApiCallService service) {
        this.service = service;
    }

    public List<ScanResult> getResults() {
        return results;
    }

    public void setResults(List<ScanResult> results) {
        this.results = results;
    }

    @Override
    public void onCreate() {
        TAG = "PwkBeacons";
        ApiCallServiceClient client = new ApiCallServiceClient(this);
        setService(client.initializeService());
        setHomeActivityClass(MainActivity.class);
        super.onCreate();
    }

    public void setServer(String server){
        editSharedPreference(SERVER, server);
    }


}
