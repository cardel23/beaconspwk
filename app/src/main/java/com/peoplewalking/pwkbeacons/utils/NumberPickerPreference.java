package com.peoplewalking.pwkbeacons.utils;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.peoplewalking.pwkbeacons.R;
import com.peoplewalking.walkingdroid.util.NumberPickerBuilder;

/**
 * Created by pwk04 on 06-29-17.
 */

public class NumberPickerPreference extends DialogPreference {

    private int mMin, mMax, mDefault, mPicker, mPickerLayout;

    private String mMaxExternalKey, mMinExternalKey;

    private NumberPicker mNumberPicker;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        NumberPickerBuilder numberPickerBuilder = new NumberPickerBuilder(view.findViewById(R.id.numberPicker2));
        numberPickerBuilder.setmDefault(mDefault);
        mNumberPicker = numberPickerBuilder.create();
    }

    @Override
    protected View onCreateDialogView() {
        LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(mPickerLayout, null);
        return view;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            if (callChangeListener(mNumberPicker.getValue()))
                super.onDialogClosed(positiveResult);
        }
    }

    public void onConfirmDialogClosed(boolean positiveResult){
        if (positiveResult) {
            setValue(mNumberPicker.getValue());
            setDefaultValue(mNumberPicker.getValue());
        }
    }

    public int getMinValue() {
        return mMin;
    }

    public void setMinValue(int mMin) {
        this.mMin = mMin;
    }

    public int getMaxValue() {
        return mMax;
    }

    public void setMaxValue(int mMax) {
        this.mMax = mMax;
    }

    public int getValue() {
        return mDefault;
    }

    public void setValue(int mDefault) {
        this.mDefault = mDefault;
    }

    public String getmMaxExternalKey() {
        return mMaxExternalKey;
    }

    public void setmMaxExternalKey(String mMaxExternalKey) {
        this.mMaxExternalKey = mMaxExternalKey;
    }

    public String getmMinExternalKey() {
        return mMinExternalKey;
    }

    public void setmMinExternalKey(String mMinExternalKey) {
        this.mMinExternalKey = mMinExternalKey;
    }

    public NumberPicker getmNumberPicker() {
        return mNumberPicker;
    }

    public void setmNumberPicker(NumberPicker mNumberPicker) {
        this.mNumberPicker = mNumberPicker;
    }

    public int getmPicker() {
        return mPicker;
    }

    public void setmPicker(int mPicker) {
        this.mPicker = mPicker;
    }

    public int getPickerLayout() {
        return mPickerLayout;
    }

    public void setPickerLayout(int mPickerLayout) {
        this.mPickerLayout = mPickerLayout;
    }

}