package com.peoplewalking.pwkbeacons.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by pwk04 on 06-07-17.
 */

public class FcmInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
//        sendRegistrationToServer(token);
    }
}
