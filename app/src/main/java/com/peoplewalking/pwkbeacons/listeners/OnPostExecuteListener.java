package com.peoplewalking.pwkbeacons.listeners;

import android.bluetooth.le.ScanResult;

import retrofit.Response;

/**
 * Created by pwk04 on 05-17-17.
 */

public interface OnPostExecuteListener<T3> extends com.peoplewalking.walkingdroid.listener.OnPostExecuteListener<T3>{
    void onPostExecute(T3 t3, Response response, StringBuilder sb, ScanResult result);
    void onPostExecute(T3 t3);
}
