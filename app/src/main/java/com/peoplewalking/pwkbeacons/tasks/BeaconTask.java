package com.peoplewalking.pwkbeacons.tasks;

import android.os.AsyncTask;

import com.peoplewalking.pwkbeacons.api.ApiCallService;
import com.peoplewalking.pwkbeacons.api.ApiCallServiceClient;
import com.peoplewalking.pwkbeacons.listeners.OnPostExecuteListener;
import com.peoplewalking.pwkbeacons.model.Beacon;

/**
 * Created by pwk04 on 05-26-17.
 */

public class BeaconTask extends AsyncTask<Void, Void, Beacon> {

    private OnPostExecuteListener<Beacon> callback;
    private String id;
    private retrofit.Response response;

    public BeaconTask(String id){
        this.id = id;
    }

    @Override
    protected Beacon doInBackground(Void... params) {
        ApiCallService service = ApiCallServiceClient.getService();
        Beacon beacon;
        try {
//            String beaconId = record.getBeaconId();
            StringBuilder sb;
            response = service.getBeacon(id).execute();
            beacon = (Beacon) response.body();
//            if(beacon != null && (beacon.getLocation() != "" || beacon.getLocation() != null))
////                sb = new StringBuilder("La ubicación de "+record.getMacAddress()+" es: "+beacon.getLocation());
//            else
//                sb = new StringBuilder("El beacon no tiene ubicación registrada");

            return beacon;
        } catch (Exception e) {
            e.printStackTrace();
//            sb = new StringBuilder("No se puede obtener la ubicación");
        }

        return null;
    }

    @Override
    protected void onPostExecute(Beacon beacon) {
        this.callback.onPostExecute(beacon);
    }

    public OnPostExecuteListener<Beacon> getCallback() {
        return callback;
    }

    public void setCallback(OnPostExecuteListener<Beacon> callback) {
        this.callback = callback;
    }

}
