package com.peoplewalking.pwkbeacons.tasks;

import android.bluetooth.le.ScanResult;
import android.os.AsyncTask;

import com.peoplewalking.pwkbeacons.api.ApiCallService;
import com.peoplewalking.pwkbeacons.api.ApiCallServiceClient;
import com.peoplewalking.pwkbeacons.listeners.OnPostExecuteListener;
import com.peoplewalking.pwkbeacons.model.AppUser;
import com.peoplewalking.pwkbeacons.model.Beacon;
import com.peoplewalking.pwkbeacons.model.Record;

import java.util.Iterator;

/**
 * Created by pwk04 on 05-22-17.
 */

public class Task extends AsyncTask<Record,Void,Beacon> {

    private OnPostExecuteListener<Beacon> callback;
    private retrofit.Response response;
    private Record record;
    private StringBuilder sb;
    private ScanResult result;

    public Task(Record record, OnPostExecuteListener onPostExecuteListener,ScanResult result){
        this.callback = onPostExecuteListener;
        this.record = record;
        this.result = result;
    }

    public Task(Record record, OnPostExecuteListener onPostExecuteListener){
        this.callback = onPostExecuteListener;
        this.record = record;
    }

    public Task(Record record, OnPostExecuteListener onPostExecuteListener, StringBuilder sb, ScanResult result){
        this.callback = onPostExecuteListener;
        this.record = record;
        this.sb = sb;
        this.result = result;
    }

    public Beacon executeSynchronous(Record... params){
        return doInBackground(params);
    }

    @Override
    protected Beacon doInBackground(Record... params) {
        ApiCallService service = ApiCallServiceClient.getService();

        try {
            response = service.getBeaconByMac(record.getMacAddress()).execute();
//            response = service.getBeacon().execute();
            Beacon beacon = (Beacon) response.body();
            if(beacon != null)
                record.setBeaconId(beacon);
            else{
                beacon = new Beacon();
                beacon.setMacAddress(record.getMacAddress());
                response = service.sendBeacon(beacon).execute();
                beacon = (Beacon) response.body();
                record.setBeaconId(beacon);
            }
            try {
                sb = new StringBuilder(beacon.getLocation().getName());
            }
            catch (Exception e){
                sb = new StringBuilder("No se puede obtener la ubicación");
            }
            AppUser user = AppUser.findById(AppUser.class, 1L);
            record.setAppUser(user);
            response = service.sendRecord(record).execute();
            return beacon;
        } catch (Exception e) {
            e.printStackTrace();
            sb = new StringBuilder("No se puede obtener la ubicación");
            return null;
        }

    }

    @Override
    protected void onPostExecute(Beacon aBeacon) {
//            Log.i("TASK",response.message());
        callback.onPostExecute(aBeacon,response,sb, result);
    }
}
