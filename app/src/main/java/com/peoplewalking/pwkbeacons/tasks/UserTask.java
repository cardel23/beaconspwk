package com.peoplewalking.pwkbeacons.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.peoplewalking.pwkbeacons.api.ApiCallService;
import com.peoplewalking.pwkbeacons.config.AppContext;
import com.peoplewalking.pwkbeacons.listeners.OnPostExecuteListener;
import com.peoplewalking.pwkbeacons.model.AppUser;

import retrofit.Response;

/**
 * Created by carlos on 05-28-17.
 */

public class UserTask extends CallbackTask<Void, Void, AppUser> {

    private OnPostExecuteListener callback;
    private AppContext app;
    private String name;
    private Response<AppUser> response;

    public UserTask(Context context, String name){
        super(context);
        this.name = name;
        this.app = (AppContext) context.getApplicationContext();
    }

    @Override
    protected AppUser doInBackground(Void... voids) {

        ApiCallService service = app.getService();
        try {
            response = service.getUser(name).execute();
            AppUser user = response.body();
            if(user == null){
                user = new AppUser();
                user.setName(name);
                response = service.sendUser(user).execute();
                user = response.body();
                if(response.code() == 200){
                    user.save();
                    return user;
                }
                else {
                    return null;
                }
            }
            else {
                user.save();
                return user;
            }
        }
        catch (Exception e){
            return null;
        }
    }

    public void setCallback(OnPostExecuteListener<AppUser> callback){
        this.callback = callback;
    }

    @Override
    protected void onPostExecute(AppUser appUser) {
        callback.onPostExecute(appUser, response, null, null);
    }
}
