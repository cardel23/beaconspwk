package com.peoplewalking.pwkbeacons.tasks;


/**
 * Created by pwk04 on 06-12-17.
 */

import java.util.ArrayList;
import java.util.HashMap;

import com.peoplewalking.pwkbeacons.R;
import com.peoplewalking.pwkbeacons.config.AppContext;
import com.peoplewalking.pwkbeacons.listeners.OnPostExecuteListener;
import com.peoplewalking.walkingdroid.task.TaskWithCallback;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public abstract class CallbackTask<T1, T2, T3> extends TaskWithCallback<T1, T2, T3> {
    protected AppContext app;
//    protected OpenbravoService service;
    protected Context context;
    protected ArrayList<String> errorMessages;
    protected OnPostExecuteListener<T3> onPostExecuteListener;
    protected ProgressDialog progressDialog;
    protected String loadingMessage;
    protected Integer timeout;
    protected Integer timer = 0;
    protected HashMap<String,String> queryParams = new HashMap<String, String>();
    protected StringBuilder query = new StringBuilder();

    public CallbackTask(Context context){
        super(context);
        this.app = (AppContext) context.getApplicationContext();
    }

    public CallbackTask(Context context, String loadingMessage){
        super(context, loadingMessage);
        this.app = (AppContext) context.getApplicationContext();

    }

    public AppContext getApp() {
        return app;
    }

    public void setApp(AppContext app) {
        this.app = app;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public OnPostExecuteListener<T3> getOnPostExecuteListener() {
        return onPostExecuteListener;
    }

    public void setOnPostExecuteListener(
            OnPostExecuteListener<T3> onPostExecuteListener) {
        this.onPostExecuteListener = onPostExecuteListener;
    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

    @Override
    public void setErrorMessages(ArrayList<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public Integer getTimeout() {
        if(timeout == null)
            timeout = 10000;
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    @Override
    protected void onPreExecute() {
//        super.onPreExecute();
        app.setLocaleConfig();
        String message;
        if(loadingMessage != null)
            message = loadingMessage;
        else
            message = app.getString(R.string.loading);
        if(showDialog) {
            progressDialog = ProgressDialog.show(this.context, "", message, true, true, new DialogInterface.OnCancelListener() {

                public void onCancel(DialogInterface dialog) {
                    // TODO Auto-generated method stub
                    progressDialog.cancel();
                    CallbackTask.this.cancel(true);
                }
            });
        }
    }

    @Override
    protected void onPostExecute(T3 response) {
//        super.onPostExecute(response);
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
//		progressDialog.dismiss();
        if(this.onPostExecuteListener != null){
            this.onPostExecuteListener.onPostExecute(response, errorMessages);
        }
    }

    @Override
    protected void onCancelled() {
        // TODO Auto-generated method stub
        super.onCancelled();
        this.cancel(true);
    }

    @Override
    protected void onProgressUpdate(T2... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        if(timer.equals(timeout))
            this.cancel(true);
    }

    protected T3 executeSynchronous(T1... params) {
        return null;
    }
}

