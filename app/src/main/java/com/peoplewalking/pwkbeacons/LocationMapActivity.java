package com.peoplewalking.pwkbeacons;

import android.bluetooth.le.ScanResult;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MarkerOptions;
import com.peoplewalking.pwkbeacons.api.ApiCallService;
import com.peoplewalking.pwkbeacons.config.AppContext;
import com.peoplewalking.pwkbeacons.fragments.CustomMapFragment;
import com.peoplewalking.pwkbeacons.listeners.OnPostExecuteListener;
import com.peoplewalking.pwkbeacons.model.Beacon;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import retrofit.Response;

public class LocationMapActivity extends FragmentActivity implements /*OnMapReadyCallback,*/
        OnPostExecuteListener<List<Beacon>> {

//    private GoogleMap mMap;
    private AppContext app;
//    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        app = (AppContext) getApplicationContext();
//        setContentView(R.layout.activity_location_map);
//        mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
////        // Add a marker in Sydney, Australia, and move the camera.
////        LatLng sydney = new LatLng(12.119611, -86.263028);
////        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
////        mMap.setMinZoomPreference(50.0f);
////        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//        BeaconsMap task = new BeaconsMap();
//        task.setCallback(this);
//        task.execute();
//
//    }
//
    @Override
    public void onPostExecute(List<Beacon> beacons, Response response, StringBuilder sb, ScanResult result) {

    }

    @Override
    public void onPostExecute(List<Beacon> beacons) {
//        try{
//            for (Beacon b :
//                    beacons) {
//                BigDecimal bdlat = new BigDecimal(12);
//                BigDecimal bdlon = new BigDecimal(1);
//
//                LatLng pos = new LatLng(bdlat.doubleValue(), bdlon.doubleValue());
//                StringBuilder sb = new StringBuilder();
//                sb.append("Beacon: ").append(b.getBeaconId()).append("\n");
//                mMap.addMarker(new MarkerOptions().position(pos).title(sb.toString()));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(pos));
//            }
//            mMap.setMinZoomPreference(10.0f);
//            mMap.setMaxZoomPreference(50.0f);
//        }
//        catch (Exception e){
//            Toast.makeText(app,"No se puede mostrar la ubicacion",Toast.LENGTH_SHORT);
//            finish();
//        }
    }
//
    @Override
    public void onPostExecute(List<Beacon> response, List<String> errorMessages) {

    }
//
//    public class BeaconsMap extends AsyncTask<Void,Void,List<Beacon>>{
//
//        private OnPostExecuteListener callback;
//
//
//        @Override
//        protected List<Beacon> doInBackground(Void... params) {
//            ApiCallService service = app.getService();
//            try {
//                List<Beacon> beacons = (List<Beacon>) service.getBeacons().execute().body();
//                return beacons;
//            } catch (NullPointerException | IOException e) {
//                e.printStackTrace();
//                return null;
//            }
////            return null;
//        }
//
//        @Override
//        protected void onPostExecute(List<Beacon> beacons) {
//            if(getCallback() != null){
//                callback.onPostExecute(beacons);
//            }
//            else super.onPostExecute(beacons);
//
//        }
//
//        public OnPostExecuteListener getCallback() {
//            return callback;
//        }
//
//        public void setCallback(OnPostExecuteListener callback) {
//            this.callback = callback;
//        }
//    }
}
