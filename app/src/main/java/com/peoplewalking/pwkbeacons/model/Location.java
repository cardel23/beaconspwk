package com.peoplewalking.pwkbeacons.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by carlos on 05-27-17.
 */

public class Location {

    @Expose
    @SerializedName("locationId")
    private String locationId;

    @Expose
    @SerializedName("locationName")
    private String name;

    @Expose
    @SerializedName("lat")
    private String latitud;

    @Expose
    @SerializedName("lon")
    private String longitud;

    @Expose
    @SerializedName("created")
    private String created;

    @Expose
    @SerializedName("updated")
    private String updated;

    @Expose
    @SerializedName("isactive")
    private String active;

    public Location(){}

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
