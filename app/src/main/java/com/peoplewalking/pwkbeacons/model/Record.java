package com.peoplewalking.pwkbeacons.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

/**
 * Created by pwk04 on 05-17-17.
 */

public class Record extends WSEntity {

    @Expose
    @SerializedName("recordId")
    private String recordId;

    @Expose
    @SerializedName("beaconId")
    private Beacon beacon;

    @Expose
    @SerializedName("isactive")
    private String isactive;

    @Expose
    @SerializedName("created")
    private String created;

    @Expose
    @SerializedName("macAddress")
    private String macAddress;

    @Expose
    @SerializedName("frameType")
    private String frameType;

    @Expose
    @SerializedName("rssi")
    private int rssi;

    @Expose
    @SerializedName("advtx")
    private int advTx;

    @Expose
    @SerializedName("appUser")
    private AppUser appUser;

    public Record(){
        recordId = UUID.randomUUID().toString().replace("-","").toUpperCase();
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getFrameType() {
        return frameType;
    }

    public void setFrameType(String frameType) {
        this.frameType = frameType;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getAdvTx() {
        return advTx;
    }

    public void setAdvTx(int advTx) {
        if(advTx == 0)
            this.advTx = 0;
        this.advTx = advTx;
    }

    public Beacon getBeaconId() {
        return beacon;
    }

    public void setBeaconId(Beacon beacon) {
        this.beacon = beacon;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }
}
