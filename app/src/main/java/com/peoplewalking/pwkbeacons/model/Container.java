package com.peoplewalking.pwkbeacons.model;

import android.bluetooth.le.ScanResult;

/**
 * Created by carlos on 05-27-17.
 */

public class Container {
    private ScanResult scanResult;
    private Beacon beacon;
    private StringBuilder stringBuilder;

    public Container(ScanResult scanResult, Beacon beacon, StringBuilder stringBuilder){
        this.setScanResult(scanResult);
        this.setBeacon(beacon);
        this.setStringBuilder(stringBuilder);
    }

    public ScanResult getScanResult() {
        return scanResult;
    }

    public void setScanResult(ScanResult scanResult) {
        this.scanResult = scanResult;
    }

    public Beacon getBeacon() {
        return beacon;
    }

    public void setBeacon(Beacon beacon) {
        this.beacon = beacon;
    }

    public StringBuilder getStringBuilder() {
        return stringBuilder;
    }

    public void setStringBuilder(StringBuilder stringBuilder) {
        this.stringBuilder = stringBuilder;
    }
}
