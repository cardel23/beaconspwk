package com.peoplewalking.pwkbeacons.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by pwk04 on 05-19-17.
 */

public class Beacon {

    @Expose
    @SerializedName("beaconId")
    private String uuid;

    @Expose
    @SerializedName("beaconServiceId")
    private String beaconId;

    @Expose
    @SerializedName("created")
    private String created;

    @Expose
    @SerializedName("updated")
    private String updated;

    @Expose
    @SerializedName("macAddress")
    private String macAddress;

    @Expose
    @SerializedName("isactive")
    private String isactive;

    @Expose
    @SerializedName("locationId")
    private Location location;

//    @Expose
//    @SerializedName("lat")
//    private String lat;
//
//    @Expose
//    @SerializedName("lon")
//    private String lon;

    public Beacon(){
        uuid = UUID.randomUUID().toString().replace("-","").toUpperCase();
    }


    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getUuid() {
        return uuid;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


}
