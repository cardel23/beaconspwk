package com.peoplewalking.pwkbeacons.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by carlos on 05-27-17.
 */

public class AppUser extends SugarRecord<AppUser> implements Serializable {

    @Expose
    @SerializedName("appUserId")
    private String appUserId;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("created")
    private String created;

    @Expose
    @SerializedName("updated")
    private String updated;

    @Expose
    @SerializedName("isactive")
    private String active;


    public String getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(String appUserId) {
        this.appUserId = appUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
