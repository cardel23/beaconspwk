package com.peoplewalking.walkingdroid.util;

/**
 * Created by pwk04 on 02-08-16.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;

public class PreferenceManager {
    public static final String PREFERENCE_DOMAIN = "com.peoplewalking.walkingdroid";
    public static final String USER_PREFERENCE = "user";
    public static final String PASSWORD_PREFERENCE = "pass";
    public static final String REMEMBER_USER_PREFERENCE = "rememberUser";
    public static final String REMEMBER_PASSWORD_PREFERENCE = "rememberPass";
    public static final String PROTOCOL_PREFERENCE = "protocol";
    public static final String SERVER_PREFERENCE = "server";
    public static final String PORT_PREFERENCE = "port";
    public static final String LOCALE_PREFERENCE = "locale";

    private Context appContext;
    private SharedPreferences sharedPreferences;
    private Locale locale;

    public PreferenceManager(Context context){
        this.appContext = context;
        sharedPreferences = this.appContext.getSharedPreferences(PREFERENCE_DOMAIN, Context.MODE_PRIVATE);
    }

    public Locale getLocale() {
        if(locale == null){
            String lang = sharedPreferences.getString(LOCALE_PREFERENCE, null);
            if(lang == null){
                editSharedPreference(LOCALE_PREFERENCE, "en");
                locale = new Locale("en");
            }
            else
                locale = new Locale(lang);
        }
        Locale.setDefault(locale);
        return Locale.getDefault();
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
        editSharedPreference(LOCALE_PREFERENCE, locale.getLanguage());
        Configuration config = this.appContext.getResources().getConfiguration();
        config.locale = getLocale();
        this.appContext.getResources().updateConfiguration(config, this.appContext.getResources().getDisplayMetrics());
    }

    public String getProtocolPreference(){
        return sharedPreferences.getString(PROTOCOL_PREFERENCE, "http");
    }

    public void setProtocolPreference(String protocolPreference){
        editSharedPreference(PROTOCOL_PREFERENCE, protocolPreference);
    }

    public String getServerPreference(){
        return sharedPreferences.getString(SERVER_PREFERENCE, null);
    }

    public void setServerPreference(String server){
        editSharedPreference(SERVER_PREFERENCE, server);
    }

    public String getSavedUserPreference(){
        return sharedPreferences.getString(USER_PREFERENCE, null);
    }

    public void setSavedUserPreference(String username){
        editSharedPreference(USER_PREFERENCE,username);
    }

    public String getSavedPassPreference(){
        return sharedPreferences.getString(PASSWORD_PREFERENCE, null);
    }

    public void setSavedPassPreference(String pass){
        editSharedPreference(PASSWORD_PREFERENCE, pass);
    }

    public Boolean getRememberUserPreference(){
        return sharedPreferences.getBoolean(REMEMBER_USER_PREFERENCE, false);
    }

    public void setRememberUserPreference(Boolean rememberUser){
        editSharedPreference(REMEMBER_USER_PREFERENCE, rememberUser);
    }

    public Boolean getRememberPassPreference(){
        return sharedPreferences.getBoolean(REMEMBER_PASSWORD_PREFERENCE, false);
    }

    public void setRememberPassPreference(Boolean rememberUser){
        editSharedPreference(REMEMBER_PASSWORD_PREFERENCE, rememberUser);
    }

    public void setApplicationPort(Integer port){
        editSharedPreference(PORT_PREFERENCE, port);
    }

    public Integer getApplicationPort(){
        Integer port = sharedPreferences.getInt(PORT_PREFERENCE, 0);
        if(port == 0){
            editSharedPreference(PORT_PREFERENCE, 80);
            port = 80;
        }
        return port;
    }

    public void editSharedPreference(String preferenceName, String val){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, val);
        editor.commit();
    }

    public void editSharedPreference(String preferenceName, int val){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(preferenceName, val);
        editor.commit();
    }

    public void editSharedPreference(String preferenceName, Boolean val){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(preferenceName, val);
        editor.commit();
    }

    public boolean isPreferenceSet(String preferenceName){
        return sharedPreferences.contains(preferenceName);
    }

}

