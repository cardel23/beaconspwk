package com.peoplewalking.walkingdroid.util;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Build;
import android.os.PowerManager;

import java.io.IOException;

/**
 * Created by Carlos Delgadillo on 08-13-15.
 */
public class FlashLEDManager {

    private static Camera mCamera;           // Reference to the Camera
    private static Camera.Parameters mParams;           // Holder for the Camera's parameters
    private static PowerManager.WakeLock mWakeLock;         // Needed for the flashlight to function

    private static boolean       isInit    = false; // Is everything set up?
    private static boolean       isFlashOn = false; // Status of FlashLight
    private Context context;
    private FlashThread flashThread;

    public FlashLEDManager(Context context){
        this.context = context;
    }

    private void initializeCamera() {

        // An WakeLock is needed to keep an hold on the Camera
        if (mWakeLock     == null) {
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "NotifFlashlight");
        }

        // Don't do anything when it is already initialized
        if (mCamera == null) {

            // Get an instance from camera
            mCamera = Camera.open();

            try {

                // Set an preview display we won't use (just for the flash to work)
                // Older devices need an display, and the newer ones, need an texture
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) mCamera.setPreviewDisplay(null);
                else                                                                mCamera.setPreviewTexture(new SurfaceTexture(0));

            } catch (IOException ioe) {

                // Handle?
                ioe.printStackTrace();

            }

            // Start the preview (so we can start doing things)
            mCamera.startPreview();
        }

        // WakeLock to keep the flash on
        mWakeLock.acquire(3600000);

    }

    /**
     * Stops the camera
     */
    private void stopCamera() {

        // Release the wakelock, we are done with the flashlight
        mWakeLock.release();

        // If we still have an reference to the camera
        if (mCamera != null) {

            // Stop it's running preview
            mCamera.stopPreview();

            // Release the camera
            mCamera.release();

            // Set it to null, just to be sure
            mCamera = null;

        }

    }

    /** Toggle the flashlight on/off status */
    private void toggleFlashLight() {

        System.err.println("#toggleFlashLight()");

        // If we have an reference to the camera
        if (mCamera != null) {

            // Get the parameters
            mParams = mCamera.getParameters();

            // Set the flash mode to the appropriate one
            mParams.setFlashMode( (isFlashOn) ? Camera.Parameters.FLASH_MODE_OFF : Camera.Parameters.FLASH_MODE_TORCH);

            // Set the Parameters to the camera
            mCamera.setParameters(mParams);

            // Flip the on state
            isFlashOn = !isFlashOn;
        }
    }

    public FlashThread getFlashThread() {
//        if(flashThread == null)
        flashThread = new FlashThread();
        return flashThread;
    }

    public FlashThread getFlashThread(int times, int delay) {
//        if(flashThread == null)
        flashThread = new FlashThread(times, delay);
        return flashThread;
    }

    public void setFlashThread(FlashThread flashThread) {
        this.flashThread = flashThread;
    }

    public class FlashThread extends Thread {
        // Default values
        int times = 3;     // Times on (off not included)
        int delay = 150;   // Time between on-off (and off-on)

        // Empty constructor, to keep default values
        public FlashThread() {}

        // Set values to use
        public FlashThread(int times, int delay) {
            this.times = times;
            this.delay = delay;
        }

        @Override
        public void run() {
            try {

                // Initialize the Camera
                initializeCamera();

                // Toggle the flashlight and wait
                for (int i=times*2; i > 0; i--) {
                    toggleFlashLight();
                    if (i != 1) sleep(delay); // Don't sleep at the last blink (It isn't needed)
                }

                // Stop the Camera
                stopCamera();
                System.err.println("FlashThread Finish");
            } catch (Exception e) {
                // Don't do anything, the user don't have to know this
            }
        }
    }
}
