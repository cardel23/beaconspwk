package com.peoplewalking.walkingdroid.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Utilidades básicas para aplicaciones Android
 * @author  Carlos Delgadillo
 * on 02-03-16.
 * @version 1
 */
public class WDUtil {

    /**
     * Crea un identificador aleatorio unico
     * @return ID hexadecimal para objetos Openbravo
     * @author Carlos Delgadillo
     * @fecha 05/08/2015
     */
    public static String generateObId(){
        UUID rawUid = UUID.randomUUID();
        String stringUid = rawUid.toString();
        String parsedObId = stringUid.replace("-", "");
        return parsedObId.toUpperCase();
    }

    /**
     * Convierte un decimal en cadena
     * @param number el decimal
     * @return el string con la cadena convertida o N/A si no es posible la conversión o el valor
     * es nulo
     */
    public static String floatToString(Float number){
        return (number != null) ? String.valueOf(number) : "N/A";
    }

    /**
     * Convierte una cadena de fecha a un formato legible
     * @param dateTimeString la cadena inicial
     * @return la cadena con formato o nulo si no puede realizar la conversión
     */
    public static String splitDateTime(String dateTimeString){
        if(dateTimeString != null){
            String[] parts = dateTimeString.split("T");
            return (parts != null && parts.length != 0) ? parts[0] : null;
        }
        return null;
    }

    /**
     * Convierte un objeto de tipo fecha a una cadena con formato legible
     * @param date el objeto fecha a convertir
     * @return la cadena con formato
     */
    public static String dateToString(Date date){
        if(date != null){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.format(date);
        }
        return null;
    }

    /**
     * Recorta una cadena de texto a un tamaño determinado
     * @param text el texto a recortar
     * @param length el tamaño deseado
     * @return la cadena recortada o una cadena vacía
     */
    public static String subString(String text, int length){
        if(text != null){
            if(text.length() > length){
                return text.substring(0, length).concat("...");
            } else{
                return text;
            }
        }
        return "";
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }
}
