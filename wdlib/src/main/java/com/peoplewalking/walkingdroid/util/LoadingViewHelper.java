package com.peoplewalking.walkingdroid.util;

import com.peoplewalking.walkingdroid.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.Toast;

public class LoadingViewHelper{
    private static final String TAG = "LoadingViewHelper";
    LinearLayout progressBarContainer;
    View container;
    ViewGroup parentContainer;
    private Context context;

    public LoadingViewHelper(Context context, View viewToHide) {
        try {
            this.context = context;
            this.container = viewToHide;
            this.progressBarContainer = (LinearLayout) LayoutInflater.from(this.context).inflate(R.layout.template_loading_progressbar, null);
            this.parentContainer = (ViewGroup) this.container.getParent();
            if(parentContainer != null){
                this.parentContainer.addView(this.progressBarContainer, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            }
        } catch (Exception e) {
            Toast.makeText(context, "Error while loading layout", Toast.LENGTH_LONG).show();
            Log.e(TAG, e.getMessage(), e);
            e.printStackTrace();
        }
    }

    public void showProgress(final boolean show) {
        if(show){
            this.replaceView(container, progressBarContainer);
        } else{
            this.replaceView(progressBarContainer, container);
        }
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void replaceView(final View viewToHide, final View view) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        try {
            if(viewToHide == null || view == null){
                throw new Exception("view and viewToHide cannot be null");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = context.getResources().getInteger(
                        android.R.integer.config_shortAnimTime);

                viewToHide.setVisibility(View.GONE);
                viewToHide.animate().setDuration(shortAnimTime).alpha(0)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                viewToHide.setVisibility(View.GONE);
                            }
                        });

                view.setVisibility(View.VISIBLE);
                view.animate().setDuration(shortAnimTime).alpha(1)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                view.setVisibility(View.VISIBLE);
                            }
                        });
            } else {
                // The ViewPropertyAnimator APIs are not available, so simply show
                // and hide the relevant UI components.
                viewToHide.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Toast.makeText(context, context.getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
            Log.e(TAG, e.getMessage());
        }
    }

}
