package com.peoplewalking.walkingdroid.util;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.orm.SugarApp;
import com.peoplewalking.walkingdroid.openbravo.model.OBModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Contexto de aplicación Android. Contiene datos genéricos de contexto útiles para las aplicaciones.
 * Es extensible a nuevos cambios.
 * @author Carlos Delgadillo
 * fecha
 * @version 1
 */
public class WDApp extends SugarApp {
    private Locale locale;
    private SharedPreferences sharedPreferences;
    private Intent homeIntent;
    private File imageDir;
    protected String server;
    protected int currentFragment;
    public static final String SERVER = "server";
    protected static String TAG = "WDApp";
    private static final String KEY_APP_CRASHED = "UnhandledException";
    private Map<String, Object> WDParams;
    protected Class homeActivityClass;
    private String User;
    protected static String appName = "WDApp";

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        imageDir = cw.getDir("images", Context.MODE_PRIVATE);
        final Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable exception) {
                // Save the fact we crashed out.
                Log.wtf(TAG,"Fallo crítico",exception);
                exception.printStackTrace();
                getSharedPreferences(TAG, Context.MODE_PRIVATE).edit()
                        .putBoolean(KEY_APP_CRASHED, true).apply();
                // Chain default exception handler.
                if (defaultHandler != null) {
                    defaultHandler.uncaughtException(thread, exception);
                }
            }
        });


        boolean bRestartAfterCrash = getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .getBoolean(KEY_APP_CRASHED, false);
        if (bRestartAfterCrash) {
            // Clear crash flag.
            getSharedPreferences(TAG, Context.MODE_PRIVATE).edit()
                    .putBoolean(KEY_APP_CRASHED, false).apply();
            // Re-launch from root activity with cleared stack.
            Intent intent = new Intent(this, getHomeActivityClass());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Toast.makeText(this, "Se ha producido un error inesperado", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Revisa si hay conexión Wi-Fi
     * @return verdadero o falso
     */
    public boolean isConnected(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * Crea un locale por defecto si no existe en las propiedades
     * @return el locale de la app
     * @author Carlos Delgadillo
     * @fecha 05/05/2015
     */
    public Locale getLocale() {
        if(locale == null){
            String lang = getPrefs().getString("locale",null);
            if(lang == null){
                editSharedPreference("locale", "es");
                locale = new Locale("es");
            }
            else
                locale = new Locale(lang);
        }
        Locale.setDefault(locale);
        return Locale.getDefault();
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
        editSharedPreference("locale", locale.getLanguage());
        setLocaleConfig();
    }

    public boolean isServerSet(){
        if(getServer() == null)
            return false;
        return true;
    }

    public String getServer(){
        server = getPrefs().getString(SERVER, null);
        return server;
    }

    public String getSavedUser(){
        String user = getPrefs().getString("savedUser", null);
        return user;
    }

    public String getSavedPass(){
        String pass = getPrefs().getString("savedPass", null);
        return pass;
    }

    public void setServer(String server){
        editSharedPreference(SERVER, server);
    }

    public void setApplicationPort(Integer port){
        editSharedPreference("defaultPort", port);
    }

    public Integer getApplicationPort(){
        Integer port = getPrefs().getInt("defaultPort", 0);
        if(port == 0){
            editSharedPreference("defaultPort", 80);
            port = 80;
        }
        return port;
    }

    public String getServerFromContext(){
        if(server == null)
            return getServer();
        else
            return server;
    }
    /**
     * Crea una instancia de SharedPreferences para la aplicación
     * @return las preferencias del usuario
     * @author Carlos Delgadillo
     * @fecha 05/05/2015
     */
    public SharedPreferences getPrefs(){
        if(sharedPreferences == null)
            sharedPreferences = getSharedPreferences(appName, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public boolean isLogged(){
        if(getUser() != null)
            return true;
        return false;
    }

    /**
     * Edita un parámetro de configuración (SharedPreference)
     * @param preferenceName nombre de la preferencia
     * @param val valor asignado
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void editSharedPreference(String preferenceName, String val){
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(preferenceName, val);
        editor.commit();
    }

    /**
     * Edita un parámetro de configuración (SharedPreference)
     * @param preferenceName
     * @param val valor asignado
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void editSharedPreference(String preferenceName, int val){
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putInt(preferenceName, val);
        editor.commit();
    }

    /**
     * Setea la configuración del locale para las actividades
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void setLocaleConfig(){
        Configuration config = this.getResources().getConfiguration();
        config.locale = getLocale();
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    /**
     * Define un intent de inicio para la navegación
     * @param intent
     * @author Carlos Delgadillo
     * fecha 15/05/2015
     */
    public void setIntent(Intent intent){
        this.homeIntent = intent;
    }

    /**
     * Retorna el intent home para navegar a la actividad principal
     * @return el intent definido
     * @author Carlos Delgadillo
     * fecha 15/05/2015
     */
    public Intent getHomeIntent(){
        return homeIntent;
    }

    /**
     * Elimina todas las entidades de un tipo específico en la BD
     * @param entity objeto
     * @param <T> el tipo de entidad
     * @author Carlos Delgadillo
     * fecha 05/02/2016
     */
    public <T extends OBModel> void clearDB(T entity){
        entity.deleteAll(entity.getClass());
    }

    public String saveImageToInternalStorage(Bitmap bitmapImage, String dir, String imageName){
        File imageDir=new File(this.imageDir, dir);
        if(!imageDir.exists()){
            imageDir.mkdirs();
        }

        File imagePath=new File(imageDir, imageName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imagePath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return relative image path
        return dir.concat("/").concat(imageName);
    }

    public Bitmap loadImageFromInternalStorage(String relativeImagePath){
        try {
            if(relativeImagePath != null && !"".equals(relativeImagePath)){
                File f=new File(this.imageDir, relativeImagePath);
                return BitmapFactory.decodeStream(new FileInputStream(f));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return BitmapFactory.decodeResource(getResources(), android.R.drawable.gallery_thumb);
    }

    public File getImageDir() {
        return imageDir;
    }

    public void setImageDir(File imageDir) {
        this.imageDir = imageDir;
    }

    /**
     * Debe setearse la clase Home por defecto en la clase que implementa este contexto,
     * de lo contrario habrá serios problemas con las excepciones no controladas.
     * @return
     */
    public Class getHomeActivityClass() {
        return homeActivityClass;
    }

    public void setHomeActivityClass(Class homeActivityClass) {
        this.homeActivityClass = homeActivityClass;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    private Map<String, Object> getParams(){
        if(WDParams == null)
            WDParams = new HashMap<>();
        return WDParams;
    }

    public void setParam(String key, Object value){
        getParams().put(key, value);
    }

    public Object getParam(String key){
       return getParams().get(key);
    }
}
