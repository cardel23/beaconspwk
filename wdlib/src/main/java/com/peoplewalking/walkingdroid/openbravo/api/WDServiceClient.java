package com.peoplewalking.walkingdroid.openbravo.api;

/**
 * Created by pwk04 on 02-05-16.
 */

import android.util.Base64;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peoplewalking.walkingdroid.util.WDApp;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Basada en la clase OBServiceClient de los proyectos de Android, hecha por Darlon
 * @author Carlos Delgadillo
 * @author Darlon Espinoza
 * @version 1
 * fecha 05/02/2016
 */
public abstract class WDServiceClient {
    private static WDApp app;
    public static String OB_SERVER;
    private static String OB_PORT;
    private String serviceUrl;
    public static String OB_URL = "http://"+OB_SERVER+"/openbravo/";
    public static final String REST_BASE_URL = "/openbravo/org.openbravo.service.json.jsonrest/";
    public static final String SHOWIMAGE_URL = OB_SERVER + "utility/ShowImage";
    private static WDOpenbravoService obService = null;

    public WDServiceClient() {
    }

    public <T extends WDApp> WDServiceClient(T context) {
        app = context;
        StringBuilder serviceUrlBuilder = new StringBuilder();

        //TODO: implementar correctamente la clase PreferenceManager
        serviceUrlBuilder
//                .append(app.getPreferenceManager().getProtocolPreference())
                .append("http://")
                .append(app.getServer())
                .append(":")
                .append(app.getApplicationPort())
                .append("/")
                .append(REST_BASE_URL);
        serviceUrl  = serviceUrlBuilder.toString();
    }

    /**
     * Debe ser sobreescrita para registrar las entidades a las que tendrán acceso los servicios
     * @return gsonbuilder
     * @author Carlos Delgadillo
     * fecha 05/02/2016
     */
    public static GsonBuilder getGsonBuilder(){
        GsonBuilder gsonBuilder = new GsonBuilder().setFieldNamingPolicy(
                FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .excludeFieldsWithoutExposeAnnotation();
        return gsonBuilder;
    }

    public static Gson buildGson(){
        Gson gson = getGsonBuilder().create();
        return gson;
    }

    public static <T extends WDOpenbravoService> WDOpenbravoService initializeService(String username,
                                                     String password, Class<T> serviceClass) {
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.setReadTimeout(40, TimeUnit.SECONDS);
        httpClient.setConnectTimeout(40, TimeUnit.SECONDS);
        Gson gson = buildGson();
        if (username != null && password != null) {
            // concatenate username and password with colon for authentication
            final String credentials = username + ":" + password;
            httpClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    Request request = chain.request();
                    Request newRequest = request.newBuilder()
                            .header("Authorization", string)
                            .header("Accept", "application/json")
                            .build();
                    Response originalResponse = chain.proceed(newRequest);

                    return originalResponse;
                }
            });

        }
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(getRestString())
                .client(httpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
        Retrofit adapter = builder.build();
        obService = adapter.create(serviceClass);
        return obService;
    }

    public static WDOpenbravoService getOBService() {
        return obService;
    }

    public final static String getOB_SERVER(){
        return app.getServerFromContext();
    }

    public final static String getOB_URL(){
        return "http://"+getOB_SERVER()+":"+getOB_PORT()+"/openbravo";
    }

    public final static String getRestString(){
        return getOB_URL() + "/org.openbravo.service.json.jsonrest/";
    }

    public final static String getOB_PORT(){
        OB_PORT = String.valueOf(app.getApplicationPort());
        return OB_PORT;
    }

    public Map map(){
        return new HashMap();
    }


}

