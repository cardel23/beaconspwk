package com.peoplewalking.walkingdroid.openbravo.api;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Intefaz de servicios REST de Openbravo
 * @author Carlos Delgadillo
 * Created on 02-03-16.
 * @version 1
 */
public interface WDOpenbravoService {
    public static final int UNAUTHORIZED = 401;
    public static final String CONTEXT_URL = "/openbravo/org.openbravo.service.json.jsonrest";

}
