package com.peoplewalking.walkingdroid.listener;

/**
 * Created by pwk04 on 02-04-16.
 */
import java.util.List;

public interface OnPostExecuteListener<T> {
    public void onPostExecute(T response, List<String> errorMessages);
}
